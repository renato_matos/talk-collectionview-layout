//
//  ViewController.swift
//  SoccerFieldPOC
//
//  Created by Leandro Silva on 07/01/18.
//  Copyright © 2018 Leandro. All rights reserved.
//

import UIKit

class ViewController: UIViewController {
    
    @IBOutlet weak var mainView: UIView!
    @IBOutlet weak var segmentedControl: UISegmentedControl!
    @IBOutlet weak var collectionView: UICollectionView!
    @IBOutlet weak var tacticImageView: UIImageView!
    
    fileprivate var layout: UICollectionViewLayout = Tactic343Layout()
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        tacticalScheme(segmentedControl)
    }
    
    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }
    
}

extension ViewController: UIScrollViewDelegate {
    
    func scrollViewDidScroll(_ scrollView: UIScrollView) {
        collectionView.contentOffset = .zero
    }
    
}

extension ViewController {
    
    @IBAction func share(_ sender: Any) {
        let image = mainView.takeSnapshot()
        let text = self.title!
        
        let activityViewController = UIActivityViewController(activityItems: [text, image], applicationActivities: [])
        present(activityViewController, animated: true)
    }
    
    @IBAction func tacticalScheme(_ sender: Any) {
        self.title = "Esquema tático \(segmentedControl.titleForSegment(at: segmentedControl.selectedSegmentIndex)!)"
        
        for item in 0..<self.collectionView.numberOfItems(inSection: 0) {
            self.collectionView.deselectItem(at: IndexPath(item: item, section: 0), animated: false)
        }
        
        if segmentedControl.selectedSegmentIndex == 1 {
            layout = Tactic352Layout()
        } else if segmentedControl.selectedSegmentIndex == 2 {
            layout = Tactic433Layout()
        } else if segmentedControl.selectedSegmentIndex == 3 {
            layout = Tactic442Layout()
        } else if segmentedControl.selectedSegmentIndex == 4 {
            layout = Tactic451Layout()
        } else if segmentedControl.selectedSegmentIndex == 5 {
            layout = Tactic532Layout()
        } else if segmentedControl.selectedSegmentIndex == 6 {
            layout = Tactic541Layout()
        } else {
            layout = Tactic343Layout()
        }
        
        self.collectionView.setCollectionViewLayout(layout, animated: true)
    }
    
}

extension ViewController: UICollectionViewDelegate {
    
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        self.collectionView.collectionViewLayout.invalidateLayout()
    }
    
}

extension ViewController: UICollectionViewDataSource {
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return 12
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "cell", for: indexPath) as! TacticViewCell
        return cell
    }
    
}
