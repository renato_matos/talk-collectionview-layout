//
//  TacticLayout.swift
//  SoccerFieldPOC
//
//  Created by Leandro Silva on 07/01/18.
//  Copyright © 2018 Leandro. All rights reserved.
//

import UIKit

class TacticLayout: UICollectionViewLayout {
    
    fileprivate var columnWidth: CGFloat = 56.0
    fileprivate var rowHeight: CGFloat = 80.0
    fileprivate let collectionViewContentInset = UIEdgeInsetsMake(0.0, -2.0, -40.0, 0.0)
    fileprivate var lastIndexPath: IndexPath? = nil
    
    internal var tactic = [3,4,3] {
        didSet {
            if tactic.count == 3 {
                tactic.insert(2, at: 0)
            }
        }
    }
    
    internal var cache = [UICollectionViewLayoutAttributes]()
    
    fileprivate var contentHeight: CGFloat {
        guard let collectionView = collectionView else { return 0 }
        let insets = collectionView.contentInset
        return collectionView.bounds.height - (insets.top + insets.bottom)
    }
    
    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else { return 0 }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
}

extension TacticLayout {
    
    override var collectionViewContentSize: CGSize {
        return CGSize(width: contentWidth, height: contentHeight)
    }
    
    override func prepare() {
        
        guard let collectionView = collectionView else { return }
        
        collectionView.contentInset = collectionViewContentInset
        collectionView.backgroundView = nil
        
        var item = 0
        var row = 1
        
        for numberOfColumns in tactic.reversed() {
            
            for column in 1...numberOfColumns {
                let indexPath = IndexPath(item: item, section: 0)
                
                let x = CGFloat(column) * ((1.0/CGFloat(numberOfColumns+1)) * contentWidth) - CGFloat(columnWidth/2)
                let y = CGFloat(row) * ((1.0/CGFloat(tactic.count+1)) * contentHeight) - CGFloat(rowHeight/2)
                
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = CGRect(x: x, y: y, width: columnWidth, height: rowHeight)
                cache.append(attributes)
                
                item = item + 1
            }
            
            row = row + 1
            
        }
        
        cache[10].frame = cache[10].frame.offsetBy(dx: -79.0, dy: 5.0)
        cache[11].frame = cache[11].frame.offsetBy(dx: -56.0, dy: 5.0)
        
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in cache {
            if attributes.frame.intersects(rect) {
                selectItem(attributes)
                visibleLayoutAttributes.append(attributes)
            }
        }
        
        if let collectionView = collectionView, collectionView.backgroundView == nil {
            for attributes in visibleLayoutAttributes {
                attributes.alpha = 1.0
            }
        }
        
        return visibleLayoutAttributes
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return cache[indexPath.item]
    }
    
}

extension TacticLayout {
    
    fileprivate func selectItem(_ attributes: UICollectionViewLayoutAttributes) {
        guard let collectionView = collectionView, let indexesPath = collectionView.indexPathsForSelectedItems, let indexPath = indexesPath.first else { return }
        
        let view = UIView()
        view.backgroundColor = .black
        view.alpha = 0.05
        
        attributes.transform = CGAffineTransform.identity
        attributes.alpha = 0.6
        
        if attributes.indexPath == indexPath {
            if lastIndexPath == indexPath {
                zoomOut(indexPath)
                collectionView.backgroundView = nil
                lastIndexPath = nil
            } else {
                zoomIn(indexPath)
                attributes.alpha = 1.0
                collectionView.backgroundView = view
                lastIndexPath = indexPath
            }
        }
    }
    
    fileprivate func zoomIn(_ indexPath: IndexPath) {
        guard let collectionView = collectionView, let cell = collectionView.cellForItem(at: indexPath) else { return }
        
        let attributes = cache[indexPath.row]
        
        attributes.transform = CGAffineTransform(scaleX: 1.5, y: 1.5)
        
        let animation = CASpringAnimation(keyPath: "transform.scale")
        animation.duration = 0.6
        animation.fromValue = 1.5
        animation.toValue = 1.7
        animation.autoreverses = false
        animation.repeatCount = 1
        animation.initialVelocity = 0.5
        animation.damping = 1.0
        
        cell.layer.add(animation, forKey: "pulse")
    }
    
    fileprivate func zoomOut(_ indexPath: IndexPath) {
        guard let collectionView = collectionView, let cell = collectionView.cellForItem(at: indexPath) else { return }
        
        let attributes = cache[indexPath.row]
        
        attributes.transform = CGAffineTransform(scaleX: 1.0, y: 1.0)
        
        let animation = CASpringAnimation(keyPath: "transform.scale")
        animation.duration = 0.6
        animation.fromValue = 1.0
        animation.toValue = 0.9
        animation.autoreverses = false
        animation.repeatCount = 1
        animation.initialVelocity = 0.5
        animation.damping = 1.0
        
        cell.layer.add(animation, forKey: "pulse")
    }
    
    fileprivate func shake(_ indexPath: IndexPath) {
        guard let collectionView = collectionView, let cell = collectionView.cellForItem(at: indexPath) else { return }
        
        let animation = CABasicAnimation(keyPath: "position")
        animation.duration = 0.02
        animation.repeatCount = 3
        animation.autoreverses = true
        animation.fromValue = NSValue(cgPoint: CGPoint(x: cell.center.x - 5.0, y: cell.center.y))
        animation.toValue = NSValue(cgPoint: CGPoint(x: cell.center.x + 5.0, y: cell.center.y))
        
        cell.layer.add(animation, forKey: "position")
    }
    
}
