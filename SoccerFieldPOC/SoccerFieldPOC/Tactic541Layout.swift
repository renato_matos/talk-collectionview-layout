//
//  Tactic541Layout.swift
//  SoccerFieldPOC
//
//  Created by Leandro Silva on 07/01/18.
//  Copyright © 2018 Leandro. All rights reserved.
//

import UIKit

class Tactic541Layout: TacticLayout {
    
    override init() {
        super.init()
        self.tactic = [5,4,1]
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
    
}

extension Tactic541Layout {
    
    override func prepare() {
        if !cache.isEmpty { return }
        
        super.prepare()
        
        cache[0].frame = cache[0].frame.offsetBy(dx: 0.0, dy: 5.0)
        cache[1].frame = cache[1].frame.offsetBy(dx: -8.0, dy: -5.0)
        cache[2].frame = cache[2].frame.offsetBy(dx: 0.0, dy: 0.0)
        cache[3].frame = cache[3].frame.offsetBy(dx: 10.0, dy: 0.0)
        cache[4].frame = cache[4].frame.offsetBy(dx: 20.0, dy: -5.0)
        cache[5].frame = cache[5].frame.offsetBy(dx: -3.0, dy: -10.0)
        cache[6].frame = cache[6].frame.offsetBy(dx: 3.0, dy: -2.0)
        cache[7].frame = cache[7].frame.offsetBy(dx: 3.0, dy: 0.0)
        cache[8].frame = cache[8].frame.offsetBy(dx: 7.0, dy: -2.0)
        cache[9].frame = cache[9].frame.offsetBy(dx: 10.0, dy: -10.0)
        
    }
    
}
