//
//  UIView+Snapshot.swift
//  SoccerFieldPOC
//
//  Created by Leandro Silva on 07/01/18.
//  Copyright © 2018 Leandro. All rights reserved.
//

import UIKit

extension UIView {
    
    func takeSnapshot() -> UIImage {
        UIGraphicsBeginImageContextWithOptions(bounds.size, false, UIScreen.main.scale)
        
        drawHierarchy(in: self.bounds, afterScreenUpdates: true)
        
        let image = UIGraphicsGetImageFromCurrentImageContext()!
        UIGraphicsEndImageContext()
        return image
    }
    
}
