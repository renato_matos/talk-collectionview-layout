//
//  UIColor.swift
//  SwitchLayout
//
//  Created by Renato Matos on 19/01/2018.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

extension UIColor {
    static func random() -> UIColor {
        let colors: [UIColor] = [.red, .blue, .green, .yellow, .purple, .orange]
        
        return colors[Int(arc4random()) % colors.count]
    }
}
