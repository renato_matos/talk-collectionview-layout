//
//  Item.swift
//  SwitchLayout
//
//  Created by Renato Matos on 19/01/2018.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

struct Item {
    var color: UIColor
}
