//
//  ViewController.swift
//  SwitchLayout
//
//  Created by Renato Matos on 19/01/2018.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

class ViewController: UICollectionViewController {
    var items = [Item]()
    
    var small: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = CGSize(width: 75, height: 75)
        
        return layout
    }()
    
    var big: UICollectionViewFlowLayout = {
        let layout = UICollectionViewFlowLayout()
        
        layout.itemSize = CGSize(width: 150, height: 150)
        
        return layout
    }()
    
    init() {
        super.init(collectionViewLayout: small)
    }
    
    required init?(coder aDecoder: NSCoder) { fatalError() }
    
    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.items = (0...30).map { _ in Item(color: .random()) }
        
        collectionView?.backgroundColor = .white
        collectionView?.register(ItemCell.self, forCellWithReuseIdentifier: "ItemCell")
    }


}

