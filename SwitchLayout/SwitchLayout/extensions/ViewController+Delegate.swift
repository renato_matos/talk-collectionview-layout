//
//  ViewController+Delegate.swift
//  SwitchLayout
//
//  Created by Renato Matos on 19/01/2018.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

extension ViewController {
    
    override func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        let newLayout = (collectionView.collectionViewLayout == self.small ? self.big : self.small)
        
        collectionView.setCollectionViewLayout(newLayout, animated: true)
    }
}
