//
//  ViewController+DataSource.swift
//  SwitchLayout
//
//  Created by Renato Matos on 19/01/2018.
//  Copyright © 2018 Concrete. All rights reserved.
//

import UIKit

extension ViewController {
    
    override func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        return self.items.count
    }
    
    override func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        let cell = collectionView.dequeueReusableCell(withReuseIdentifier: "ItemCell", for: indexPath)
        
        cell.backgroundColor = self.items[indexPath.item].color
        
        return cell
    }
}
