//
//  PlayerCell.swift
//  ConcreteFC
//
//  Created by Renato Matos on 03/01/18.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import UIKit
import SnapKit

class PlayerCell: UICollectionViewCell {
    
    static let widthItem = 60
    
    var player: Player? {
        didSet {
            configPlayer()
        }
    }
    
    let image: UIImageView = {
        
        let img = UIImageView(frame: .zero)
        
        img.layer.cornerRadius = 30
        img.clipsToBounds = true
        
        return img
    }()
    
    func setup() {
        
        self.backgroundColor = UIColor.white
        self.layer.cornerRadius = 30
        self.clipsToBounds = true
        
        self.addSubview(self.image)
        
        self.image.snp.makeConstraints { (constraint) in
            constraint.top.equalTo(self.snp.top)
            constraint.bottom.equalTo(self.snp.bottom)
            constraint.leading.equalTo(self.snp.leading)
            constraint.trailing.equalTo(self.snp.trailing)
        }
    }
    
    private func configPlayer() {
        
        self.setup()
        
        if let player = self.player {
            self.image.image = UIImage(named: player.imagePlayer)
        }
    }
    
}
