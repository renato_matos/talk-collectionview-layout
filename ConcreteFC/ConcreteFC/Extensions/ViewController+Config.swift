//
//  ViewController+Config.swift
//  ConcreteFC
//
//  Created by Renato Matos on 03/01/18.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import UIKit

extension ViewController {
    
    func configViews() {
        
        self.currentLayout = SoccerLayout(config: self.formations[0])
        self.collectionView = UICollectionView(frame: .zero, collectionViewLayout: SoccerLayout(config: self.formations[0]))
        self.collectionView?.backgroundColor = UIColor.gray
        
        self.collectionView?.register(PlayerCell.self, forCellWithReuseIdentifier: "PlayerCell")
        
        self.collectionView?.dataSource = self
        self.collectionView?.delegate = self
        
        self.button.setTitle("Mudar", for: .normal)
        self.button.backgroundColor = UIColor.black
        self.button.addTarget(self, action: #selector(actionButton), for: .touchUpInside)
    }
    
    @objc func actionButton() {
        
        self.layoutSelected = self.layoutSelected < self.formations.count-1 ? self.layoutSelected + 1 : 0
        
        self.currentLayout = SoccerLayout(config: self.formations[self.layoutSelected])
        
        OperationQueue.main.addOperation {
            self.collectionView!.setCollectionViewLayout(self.currentLayout!, animated: true, completion: { (finished) in
                if finished {
                    //
                }
            })
        }
        
        
    }
}
