//
//  ViewController+Constraints.swift
//  ConcreteFC
//
//  Created by Renato Matos on 03/01/18.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import UIKit

extension ViewController {
    
    func configConstraints() {
        self.collectionView!.snp.makeConstraints { (constraint) in
            constraint.top.equalTo(self.view.snp.top)
            constraint.bottom.equalTo(self.view.snp.bottom)
            constraint.leading.equalTo(self.view.snp.leading)
            constraint.trailing.equalTo(self.view.snp.trailing)
        }
        
        self.button.snp.makeConstraints { (constraint) in
            constraint.bottom.equalTo(self.view.snp.bottom).offset(-20)
            constraint.leading.equalTo(self.view.snp.leading).offset(20)
            constraint.trailing.equalTo(self.view.snp.trailing).offset(-20)
            constraint.height.equalTo(50)
        }
        
    }
}

