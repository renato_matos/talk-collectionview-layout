//
//  ViewController.swift
//  ConcreteFC
//
//  Created by Renato Matos on 03/01/18.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import UIKit

class ViewController: UIViewController, CodableView {
    
    var currentLayout: UICollectionViewLayout?
    
    var collectionView: UICollectionView?
    
    let button = UIButton()
    
    var layoutSelected = 0
    
    let formations: [[Int]] = [
        [2, 4, 4],
        [6, 4],
        [1, 4, 1, 4],
        [2, 5, 3],
        [1, 5, 4],
        [3, 4, 3],
        [3, 3, 4],
        [2, 3, 5],
        [4, 2, 4]
    ]

    override func viewDidLoad() {
        super.viewDidLoad()
        
        self.setup()
    }

    override func didReceiveMemoryWarning() {
        super.didReceiveMemoryWarning()
    }


}

