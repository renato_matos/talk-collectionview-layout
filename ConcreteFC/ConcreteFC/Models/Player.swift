//
//  Player.swift
//  ConcreteFC
//
//  Created by Renato Matos on 05/01/2018.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import Foundation

class Player {
    
    var imagePlayer: String
    var namePlayer: String
    
    init(image: String, name: String) {
        imagePlayer = image
        namePlayer = name
    }
    
}
