//
//  SoccerLayout.swift
//  ConcreteFC
//
//  Created by Renato Matos on 11/01/2018.
//  Copyright © 2018 Studio WO. All rights reserved.
//

import UIKit

class SoccerLayout: UICollectionViewLayout {
    
    weak var delegate: ViewController!
    
    fileprivate var cache = [UICollectionViewLayoutAttributes]()
    
    fileprivate var contentWidth: CGFloat {
        guard let collectionView = collectionView else {
            return 0
        }
        let insets = collectionView.contentInset
        return collectionView.bounds.width - (insets.left + insets.right)
    }
    
    fileprivate var contentHeight: CGFloat = 0
    
    var sectionsConfig:[Int]
    
    override var collectionViewContentSize: CGSize {
        
        return CGSize(width: contentWidth, height: contentHeight)
        
    }
    
    var scrollDirection: UICollectionViewScrollDirection = .vertical
    
    init(config: [Int]) {
        sectionsConfig = config
        super.init()
    }
    
    required init?(coder aDecoder: NSCoder) {
        fatalError("init(coder:) has not been implemented")
    }
    
    override func prepare() {
        
        var indexCount = 0
        
        guard cache.isEmpty == true, let collectionView = collectionView else {
            return
        }
        
        let widthTotal: Int = Int(collectionView.frame.size.width)
        var currentY: Int = 0
        
        for section in 0 ..< self.sectionsConfig.count {
            
            let sizeItem: Int = PlayerCell.widthItem
            let numItems: Int = self.sectionsConfig[section]
            var currentX: Int = 0
            let widthTotalItems: Int = sizeItem * numItems
            
            let posItemY = currentY + sizeItem + 10
            
            for _ in 0 ..< numItems {
                
                let indexPath = IndexPath(item: indexCount, section: 0)
                
                let posItemX = currentX == 0 ? (widthTotal/2) - (widthTotalItems/2) : currentX+60
                
                let frame = CGRect(x: posItemX, y: posItemY, width: sizeItem, height: sizeItem)
                
                let attributes = UICollectionViewLayoutAttributes(forCellWith: indexPath)
                attributes.frame = frame
                cache.append(attributes)
                
                contentHeight = max(contentHeight, frame.maxY)
                
                currentX = posItemX
                
                indexCount += 1
                
            }
            
            currentY = posItemY
        }
        
        
    }
    
    override func layoutAttributesForElements(in rect: CGRect) -> [UICollectionViewLayoutAttributes]? {
        
        var visibleLayoutAttributes = [UICollectionViewLayoutAttributes]()
        
        for attributes in self.cache {
            if attributes.frame.intersects(rect) {
                visibleLayoutAttributes.append(attributes)
            }
        }
        return visibleLayoutAttributes
        
    }
    
    override func layoutAttributesForItem(at indexPath: IndexPath) -> UICollectionViewLayoutAttributes? {
        return self.cache[indexPath.item]
    }
    
    override func shouldInvalidateLayout(forBoundsChange newBounds: CGRect) -> Bool {
        if scrollDirection == .vertical, let oldWidth = collectionView?.bounds.width {
            return oldWidth != newBounds.width
        } else if scrollDirection == .horizontal, let oldHeight = collectionView?.bounds.height {
            return oldHeight != newBounds.height
        }
        
        return false
    }
    
    override func invalidateLayout() {
        super.invalidateLayout()
        
    }
}
